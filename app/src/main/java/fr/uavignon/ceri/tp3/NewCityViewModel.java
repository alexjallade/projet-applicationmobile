package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.MuseumItem;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class NewCityViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private MuseumRepository repository;
    private MutableLiveData<MuseumItem> city;

    public NewCityViewModel (Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        city = new MutableLiveData<>();
    }


}
