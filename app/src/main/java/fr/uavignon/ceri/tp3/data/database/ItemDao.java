package fr.uavignon.ceri.tp3.data.database;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.List;

import fr.uavignon.ceri.tp3.data.MuseumItem;

@Dao
public interface ItemDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(MuseumItem item);

    @Query("SELECT * from tableItems")
    List<MuseumItem> getInfo();

    @Query("SELECT * from tableItems ORDER BY name")
    List<MuseumItem> sort();

    @Query("SELECT * from tableItems ORDER BY year")
    List<MuseumItem> sortYear();

    @Query("SELECT * from tableItems WHERE _id = :id")
    MuseumItem getItemById(double id);

    @Query("SELECT * from tableItems WHERE name LIKE :req")
    List<MuseumItem> getItemBySearch(String req);


}
