package fr.uavignon.ceri.tp3;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.MuseumItem;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class MainActivity extends AppCompatActivity {

    private MainViewModel viewModel;
    EditText Search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.loadCollection();
        Log.e("yes", "yew");

    }

    public void getTextFromSearchBar (View view){
        String requete = "%";
        requete += ((EditText)findViewById(R.id.inputItem)).getText().toString();
        requete += "%";
        viewModel.getItemBySearch(requete);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update_all) {
            viewModel.loadCollection();
            viewModel.getCollection();
            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Interrogation à faire du service web",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return true;
        }
        if (id == R.id.action_sort_alpha) {
            viewModel.sortAlpha();
            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Trie par ordre alphabetique",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return true;
        }

        if (id == R.id.action_sort_year) {
            viewModel.sortYears();
            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Trie par années",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return true;
        }



        return super.onOptionsItemSelected(item);
    }
}