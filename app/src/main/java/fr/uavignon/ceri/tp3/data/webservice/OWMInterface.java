package fr.uavignon.ceri.tp3.data.webservice;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OWMInterface {
    @Headers("Accept: application/json")
    @GET("collection")
    Call <Map< String , ItemResponse >> getCollection();
}

