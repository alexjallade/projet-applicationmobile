package fr.uavignon.ceri.tp3.data.webservice;

public class Forecast {
    public final String description;
    public final String name;
    public final boolean working;
    public final String brand;
    public final int categories;


    public Forecast(String description, String name, boolean working, String brand, int categories) {
        this.description=description;
        this.name = name;
        this.working=working;
        this.brand=brand;
        this.categories=categories;
    }
}
