package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.MuseumItem;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private MuseumRepository repository;
    private MutableLiveData<MuseumItem> item;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable>webServiceThrowable;

    public DetailViewModel (Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        item = new MutableLiveData<>();
        isLoading = new MutableLiveData<>(false);
        webServiceThrowable = new MutableLiveData<>();
    }


    public MutableLiveData<MuseumItem> getItem() {
        return item;
    }

    public void setItem(long id){
        repository.getItem(id);
        item = repository.getSelectedItem();
    }
    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;}

    public MutableLiveData<Throwable> getWebServiceThrowable(){
        return webServiceThrowable;}



}

