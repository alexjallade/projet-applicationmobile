package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.MuseumItem;
import fr.uavignon.ceri.tp3.data.MuseumRepository;
import fr.uavignon.ceri.tp3.data.database.ItemDao;

import static java.lang.Thread.sleep;

public class MainViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private MuseumRepository repository;
    private int nbitems=0;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<ArrayList<MuseumItem>> backup= new MutableLiveData<>();

    public MainViewModel (Application application) {
        super(application);
        repository = MuseumRepository.get(application);
    }

    public void loadCollection() {
        repository.getCollection();
    }

    LiveData<ArrayList<MuseumItem>>  getCollection() {
        backup=repository.getCollectionDataBase();
        return backup;
    }


    public void getItemBySearch(String requete){ repository.getItemBySearch(requete);}

    public void sortAlpha() {
        repository.sortAlpha();
    }

    public void sortYears() {
        repository.sortYear();
    }




}
