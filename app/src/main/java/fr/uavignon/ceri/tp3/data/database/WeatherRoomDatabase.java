package fr.uavignon.ceri.tp3.data.database;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp3.data.MuseumItem;


@Database(entities = {MuseumItem.class}, version = 5, exportSchema = true)
public abstract class WeatherRoomDatabase extends RoomDatabase {

    private static final String TAG = WeatherRoomDatabase.class.getSimpleName();

    public abstract ItemDao itemDao();

    private static WeatherRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static WeatherRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (WeatherRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    // without populate

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    WeatherRoomDatabase.class,"tableItems")
                                    .fallbackToDestructiveMigration()
                                    .build();



                    // with populate
                     /*
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    WeatherRoomDatabase.class,"tableItems")
                                    .addCallback(sRoomDatabaseCallback)
                                    .build();
                        */
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {

                    });

                }
            };



}
