package fr.uavignon.ceri.tp3.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Dao;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "tableItems", indices = {@Index(value = {"name"}, unique = true)})
@Dao
public class MuseumItem {

    public static final String TAG = MuseumItem.class.getSimpleName();

    public static final long ADD_ID = -1;


    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="_id")
    private long id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name="icon")
    private String icon;


    /*


    @ColumnInfo(name="categories")
    private String categories;
    */
    @NonNull
    @ColumnInfo(name="brand")
    private String brand;

    @NonNull
    @ColumnInfo(name="year")
    private int year;

    @NonNull
    @ColumnInfo(name="categorie")
    private String categorie;



    @ColumnInfo(name="working")
    private Boolean working;



    public MuseumItem() {
        //this.categories="test";
        //this.marque="yes";
        this.working = false;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
    public Boolean getWorking() {
        return working;
    }

    public void setBrand(String brand) {
        this.brand=brand;
    }

    @NonNull
    public String getBrand() {
        return brand;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getCategorie() {
        return categorie;
    }



    /*
    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque=marque;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories=categories;
    }
    */
    public void setId(long id) {
        this.id=id;
    }

    public void setName(String name) {
        this.name=name;
    }

    public void setDescription(String description) {
        this.description=description;
    }
    public void setWorking(boolean working) {
        this.working=working;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }






}
