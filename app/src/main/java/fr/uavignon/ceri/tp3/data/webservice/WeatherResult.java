package fr.uavignon.ceri.tp3.data.webservice;

import android.util.Log;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.MuseumItem;

public class WeatherResult {
    public static void transferInfo(ItemResponse weatherInfo, ArrayList<MuseumItem> list,String key){

        MuseumItem item = new MuseumItem();
        item.setName(weatherInfo.name);
        item.setDescription(weatherInfo.description);
        if(weatherInfo.working!=null) {
            item.setWorking(weatherInfo.working);
        }
        if (weatherInfo.brand!=null){
            item.setBrand(weatherInfo.brand);
        }
        if (weatherInfo.year!=null){
            item.setYear(weatherInfo.year);
        }
        if(weatherInfo.pictures != null) {
            int i = 0;
            for (String img : weatherInfo.pictures.keySet()) {
                if (i == 0){
                    item.setIcon("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+key+"/images/"+img);
                }
                i+=1;
            }
        }

        if(weatherInfo.categories != null){
            String categorie = "";
            for(int i = 0; i< weatherInfo.categories.size(); i++){
                categorie = categorie + ", " + weatherInfo.categories.get(i);
            }
            String categories = categorie.substring(2);
            //System.out.println(categories);
            item.setCategorie(categories);
        }


        //item.setCategories("yes");
        //item.setMarque("marque");
        list.add(item);

    }
}
