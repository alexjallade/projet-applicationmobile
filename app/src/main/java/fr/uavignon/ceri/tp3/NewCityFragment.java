package fr.uavignon.ceri.tp3;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp3.data.MuseumItem;

public class NewCityFragment extends Fragment {


    public static final String TAG = NewCityFragment.class.getSimpleName();

    private NewCityViewModel viewModel;
    private EditText editNewName, editNewCountry;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(NewCityViewModel.class);

        // Get selected city
        NewCityFragmentArgs args = NewCityFragmentArgs.fromBundle(getArguments());
        long cityID = args.getCityNum();
        Log.d(TAG, "selected id=" + cityID);
        //viewModel.setCity(cityID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {


    }

    private void observerSetup() {


    }
}
