package fr.uavignon.ceri.tp3.data;


import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReferenceArray;

//import fr.uavignon.ceri.tp3.data.database.CityDao;
import fr.uavignon.ceri.tp3.data.database.ItemDao;
import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import fr.uavignon.ceri.tp3.data.webservice.ItemResponse;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase.databaseWriteExecutor;
import static java.lang.Thread.sleep;

public class MuseumRepository {

    private static final String TAG = MuseumRepository.class.getSimpleName();
    public static volatile int nbAPIloads = 1;
    private LiveData<List<MuseumItem>> allCities;
    private MutableLiveData<MuseumItem> selectedCity;
    private final OWMInterface api;
    //private CityDao cityDao;
    ArrayList<MuseumItem> ListItems = new ArrayList<>();
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<Boolean>(false);
    private MutableLiveData<Throwable> webServiceThrowable;
    private List<MuseumItem> synCities;
    private ItemDao itemDao;
    private int i =0;
    private static volatile MuseumRepository INSTANCE;
    private MutableLiveData<ArrayList<MuseumItem>> backup= new MutableLiveData<>();
    private MutableLiveData<MuseumItem> selectedItem = new MutableLiveData<>();

    public synchronized static MuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseumRepository(application);
        }

        return INSTANCE;
    }


    public MuseumRepository(Application application) {
        WeatherRoomDatabase db = WeatherRoomDatabase.getDatabase(application);
        itemDao = db.itemDao();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        this.api = retrofit.create(OWMInterface.class);
    }

    public MutableLiveData<MuseumItem> getSelectedItem() {

        return selectedItem;
    }


    public void loadCollection() {
        Log.e("ty", "start");
        isLoading.postValue(false);


        api.getCollection().enqueue(
                new Callback<Map<String, ItemResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ItemResponse>> call, Response<Map<String, ItemResponse>> response) {

                        Log.d("API", response.body().toString());
                        for (String key : response.body().keySet()) {
                            System.out.println(key + "=" + response.body().get(key));
                            WeatherResult.transferInfo(response.body().get(key), ListItems, key);
                            insertItem(ListItems.get(i));

                            i=i+1;
                            Log.e("ty", "boucle");
                        }
                        backup.setValue(ListItems);
                        isLoading.postValue(true);
                    }

                    @Override
                    public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {
                        Log.e("ty", "fail");
                    }
                });

    }

    public MutableLiveData<ArrayList<MuseumItem>> getCollectionDataBase(){
        upDateBackup();
        return backup;
    }

    public void upDateBackup(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<MuseumItem> save = new ArrayList<>();
                List<MuseumItem> save1 = itemDao.getInfo();
                for(int i=0; i<save1.size();i++){
                    save.add(save1.get(i));
                }
                backup.postValue(save);
            }
        }).start();
    }

    public void getItemBySearch(String req){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<MuseumItem> save = new ArrayList<>();
                List<MuseumItem> save1 = itemDao.getItemBySearch(req);
                for(int i=0; i<save1.size();i++){
                    save.add(save1.get(i));
                }
                backup.postValue(save);
            }
        }).start();
    }

    public void sortAlpha(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<MuseumItem> save = new ArrayList<>();
                List<MuseumItem> save1 = itemDao.sort();
                for(int i=0; i<save1.size();i++){
                    save.add(save1.get(i));
                }
                backup.postValue(save);
            }
        }).start();
    }

    public void sortYear(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<MuseumItem> save = new ArrayList<>();
                List<MuseumItem> save1 = itemDao.sortYear();
                for(int i=0; i<save1.size();i++){
                    save.add(save1.get(i));
                }
                backup.postValue(save);
            }
        }).start();
    }



    public void getCollection(){
       new Thread(new Runnable() {
           @Override
           public void run() {
               loadCollection();
           }
       }).start();
   }

    public void insertItem(MuseumItem newItem) {
        databaseWriteExecutor.execute(() -> {
            itemDao.insert(newItem);
        });

    }
    public void getItem(long id)  {
        new Thread(new Runnable() {
            @Override
            public void run() {
                MuseumItem save = itemDao.getItemById(id);
                //Log.e("slect0",save.getName());
                selectedItem.postValue(save);
            }
        }).start();
    }




    public MutableLiveData<Boolean> getIsLoading(){
        return isLoading;
    }
    public MutableLiveData<Throwable> getWebServiceThrowable() {return webServiceThrowable;}







}
