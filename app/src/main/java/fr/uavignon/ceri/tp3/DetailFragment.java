package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textName, textYear, textBrand, textWorking, textDescription, textCategorie;
    private ImageView imgWeather;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);


        // Get selected city
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        long cityID = args.getCityNum();
        Log.d(TAG,"selected id="+cityID);
        viewModel.setItem(cityID);
        listenerSetup();
        observerSetup(view);

    }


    private void listenerSetup() {

        textName = getView().findViewById(R.id.nameItem);
        textYear = getView().findViewById(R.id.year);
        textBrand= getView().findViewById(R.id.brand);
        textWorking = getView().findViewById(R.id.working);
        textDescription = getView().findViewById(R.id.description);
        textCategorie = getView().findViewById(R.id.categorie);
        imgWeather = getView().findViewById(R.id.iconeWeather);

        progress = getView().findViewById(R.id.progress);



        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup(View view) {
        viewModel.getItem().observe(getViewLifecycleOwner(),
                item -> {
                    if (item != null) {
                        Log.d(TAG, item.getName());
                        textName.setText(item.getName());
                        textYear.setText("Année: "+String.valueOf(item.getYear()));
                        textBrand.setText("Marque: "+item.getBrand());
                        if(item.getWorking()){
                            textWorking.setText("Fonctionnel: oui");
                        }
                        else{
                            textWorking.setText("Fonctionnel: non");
                        }

                        textDescription.setText("Description :"+item.getDescription());

                        textCategorie.setText("Categorie (s) :"+item.getCategorie());

                        if (item.getIcon()!=null){
                            Glide.with(this)
                                    .load(item.getIcon())
                                    .into(imgWeather);
                        }
                    }
                });


        viewModel.getIsLoading().observe(getViewLifecycleOwner(),
                isLoading -> {
                    if(isLoading==Boolean.TRUE){
                        progress.setVisibility(View.VISIBLE);
                        progress.setIndeterminate(true);
                    }
                    else{
                        progress.setVisibility(View.INVISIBLE);
                    }
                });

        viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(),
                webServiceThrowable-> {
                    if( webServiceThrowable!=null){
                        Snackbar.make(view, "Impossible de charger les données", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }

        });
    }


}