package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import fr.uavignon.ceri.tp3.data.MuseumItem;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class ListViewModel extends AndroidViewModel {
    private MuseumRepository repository;
    private MutableLiveData<ArrayList<MuseumItem>> itemList;
    private LiveData<List<MuseumItem>> allCities;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable>webServiceThrowable;


    public ListViewModel (Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        isLoading = new MutableLiveData<>(false);
        webServiceThrowable = new MutableLiveData<>();
        itemList = new MutableLiveData<>();
        itemList = repository.getCollectionDataBase();
    }

    public void setVariable() {
        webServiceThrowable = repository.getWebServiceThrowable();
    }


    public MutableLiveData<Boolean> getIsLoading(){
        return isLoading;
    }

    LiveData<ArrayList<MuseumItem>> getAllItems() {
        return itemList;
    }






}
